# Diff Details

Date : 2023-07-08 01:39:07

Directory c:\\Users\\minht\\OneDrive\\Desktop\\webtechnology

Total : 19 files,  -193 codes, -8 comments, -11 blanks, all -212 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [frontend/src/pages/admin/[id].js](/frontend/src/pages/admin/%5Bid%5D.js) | JavaScript | -3 | 0 | 0 | -3 |
| [frontend/src/pages/admin/components/FeatureBar/index.js](/frontend/src/pages/admin/components/FeatureBar/index.js) | JavaScript | 2 | 0 | 0 | 2 |
| [frontend/src/pages/admin/idComponents/Detail/components/Category.js](/frontend/src/pages/admin/idComponents/Detail/components/Category.js) | JavaScript | 0 | 0 | 1 | 1 |
| [frontend/src/pages/admin/idComponents/Detail/components/MarkdownEditor.js](/frontend/src/pages/admin/idComponents/Detail/components/MarkdownEditor.js) | JavaScript | 1 | 0 | 0 | 1 |
| [frontend/src/pages/admin/idComponents/Setting/index.js](/frontend/src/pages/admin/idComponents/Setting/index.js) | JavaScript | -7 | 0 | -1 | -8 |
| [frontend/src/pages/admin/idComponents/Testcase/index.js](/frontend/src/pages/admin/idComponents/Testcase/index.js) | JavaScript | -3 | -8 | 0 | -11 |
| [frontend/src/pages/authentication/index.js](/frontend/src/pages/authentication/index.js) | JavaScript | -1 | 0 | 0 | -1 |
| [frontend/src/pages/problems/components/ProblemList/Difficulty.js](/frontend/src/pages/problems/components/ProblemList/Difficulty.js) | JavaScript | 9 | 0 | 4 | 13 |
| [frontend/src/pages/problems/components/ProblemList/index.js](/frontend/src/pages/problems/components/ProblemList/index.js) | JavaScript | 0 | 0 | 1 | 1 |
| [frontend/src/pages/problems/idComponent/console/components/ConsoleBar.js](/frontend/src/pages/problems/idComponent/console/components/ConsoleBar.js) | JavaScript | -1 | 0 | 0 | -1 |
| [frontend/src/pages/problems/idComponent/console/components/TestCaseResult.js](/frontend/src/pages/problems/idComponent/console/components/TestCaseResult.js) | JavaScript | 8 | 0 | 0 | 8 |
| [frontend/src/pages/problems/idComponent/editor/index.js](/frontend/src/pages/problems/idComponent/editor/index.js) | JavaScript | 6 | 0 | 2 | 8 |
| [frontend/src/pages/problems/idComponent/result/index.js](/frontend/src/pages/problems/idComponent/result/index.js) | JavaScript | -1 | 0 | 0 | -1 |
| [gateway(backup)/version1-kong-with-db-and-konga/README.md](/gateway(backup)/version1-kong-with-db-and-konga/README.md) | Markdown | -6 | 0 | -2 | -8 |
| [gateway(backup)/version1-kong-with-db-and-konga/docker-compose.yml](/gateway(backup)/version1-kong-with-db-and-konga/docker-compose.yml) | YAML | -92 | 0 | -6 | -98 |
| [gateway(backup)/version2-kong-dbless/Dockerfile](/gateway(backup)/version2-kong-dbless/Dockerfile) | Docker | -5 | 0 | -2 | -7 |
| [gateway(backup)/version2-kong-dbless/README.md](/gateway(backup)/version2-kong-dbless/README.md) | Markdown | -6 | 0 | -2 | -8 |
| [gateway(backup)/version2-kong-dbless/declarative/kong.yml](/gateway(backup)/version2-kong-dbless/declarative/kong.yml) | YAML | -58 | 0 | -3 | -61 |
| [gateway(backup)/version2-kong-dbless/docker-compose.yml](/gateway(backup)/version2-kong-dbless/docker-compose.yml) | YAML | -36 | 0 | -3 | -39 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details