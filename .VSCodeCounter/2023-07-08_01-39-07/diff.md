# Diff Summary

Date : 2023-07-08 01:39:07

Directory c:\\Users\\minht\\OneDrive\\Desktop\\webtechnology

Total : 19 files,  -193 codes, -8 comments, -11 blanks, all -212 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 13 | 10 | -8 | 7 | 9 |
| Docker | 1 | -5 | 0 | -2 | -7 |
| Markdown | 2 | -12 | 0 | -4 | -16 |
| YAML | 3 | -186 | 0 | -12 | -198 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 19 | -193 | -8 | -11 | -212 |
| frontend | 13 | 10 | -8 | 7 | 9 |
| frontend\\src | 13 | 10 | -8 | 7 | 9 |
| frontend\\src\\pages | 13 | 10 | -8 | 7 | 9 |
| frontend\\src\\pages\\admin | 6 | -10 | -8 | 0 | -18 |
| frontend\\src\\pages\\admin (Files) | 1 | -3 | 0 | 0 | -3 |
| frontend\\src\\pages\\admin\\components | 1 | 2 | 0 | 0 | 2 |
| frontend\\src\\pages\\admin\\components\\FeatureBar | 1 | 2 | 0 | 0 | 2 |
| frontend\\src\\pages\\admin\\idComponents | 4 | -9 | -8 | 0 | -17 |
| frontend\\src\\pages\\admin\\idComponents\\Detail | 2 | 1 | 0 | 1 | 2 |
| frontend\\src\\pages\\admin\\idComponents\\Detail\\components | 2 | 1 | 0 | 1 | 2 |
| frontend\\src\\pages\\admin\\idComponents\\Setting | 1 | -7 | 0 | -1 | -8 |
| frontend\\src\\pages\\admin\\idComponents\\Testcase | 1 | -3 | -8 | 0 | -11 |
| frontend\\src\\pages\\authentication | 1 | -1 | 0 | 0 | -1 |
| frontend\\src\\pages\\problems | 6 | 21 | 0 | 7 | 28 |
| frontend\\src\\pages\\problems\\components | 2 | 9 | 0 | 5 | 14 |
| frontend\\src\\pages\\problems\\components\\ProblemList | 2 | 9 | 0 | 5 | 14 |
| frontend\\src\\pages\\problems\\idComponent | 4 | 12 | 0 | 2 | 14 |
| frontend\\src\\pages\\problems\\idComponent\\console | 2 | 7 | 0 | 0 | 7 |
| frontend\\src\\pages\\problems\\idComponent\\console\\components | 2 | 7 | 0 | 0 | 7 |
| frontend\\src\\pages\\problems\\idComponent\\editor | 1 | 6 | 0 | 2 | 8 |
| frontend\\src\\pages\\problems\\idComponent\\result | 1 | -1 | 0 | 0 | -1 |
| gateway(backup) | 6 | -203 | 0 | -18 | -221 |
| gateway(backup)\\version1-kong-with-db-and-konga | 2 | -98 | 0 | -8 | -106 |
| gateway(backup)\\version2-kong-dbless | 4 | -105 | 0 | -10 | -115 |
| gateway(backup)\\version2-kong-dbless (Files) | 3 | -47 | 0 | -7 | -54 |
| gateway(backup)\\version2-kong-dbless\\declarative | 1 | -58 | 0 | -3 | -61 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)