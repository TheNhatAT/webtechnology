# Summary

Date : 2023-07-08 00:27:07

Directory c:\\Users\\minht\\OneDrive\\Desktop\\webtechnology

Total : 180 files,  36782 codes, 247 comments, 1174 blanks, all 38203 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 11 | 28,500 | 0 | 11 | 28,511 |
| JavaScript | 132 | 7,178 | 209 | 991 | 8,378 |
| YAML | 9 | 383 | 0 | 33 | 416 |
| XML | 3 | 340 | 0 | 0 | 340 |
| CSS | 1 | 131 | 1 | 22 | 154 |
| Lua | 2 | 62 | 7 | 23 | 92 |
| Properties | 3 | 48 | 18 | 17 | 83 |
| Docker | 6 | 40 | 0 | 29 | 69 |
| Markdown | 3 | 35 | 0 | 20 | 55 |
| Protocol Buffers | 2 | 26 | 0 | 8 | 34 |
| Batch | 1 | 17 | 0 | 7 | 24 |
| C++ | 1 | 8 | 0 | 3 | 11 |
| Shell Script | 1 | 7 | 8 | 8 | 23 |
| JSON with Comments | 1 | 4 | 4 | 0 | 8 |
| Ignore | 2 | 2 | 0 | 0 | 2 |
| Go | 1 | 1 | 0 | 1 | 2 |
| TypeScript JSX | 1 | 0 | 0 | 1 | 1 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 180 | 36,782 | 247 | 1,174 | 38,203 |
| .idea | 1 | 338 | 0 | 0 | 338 |
| backend | 81 | 15,433 | 172 | 604 | 16,209 |
| backend (Files) | 3 | 30 | 8 | 16 | 54 |
| backend\\apiGateway | 5 | 155 | 7 | 33 | 195 |
| backend\\apiGateway (Files) | 2 | 41 | 0 | 7 | 48 |
| backend\\apiGateway\\kong | 2 | 62 | 7 | 23 | 92 |
| backend\\apiGateway\\kong-config | 1 | 52 | 0 | 3 | 55 |
| backend\\apiGateway\\kong\\plugins | 2 | 62 | 7 | 23 | 92 |
| backend\\apiGateway\\kong\\plugins\\custom-authen | 2 | 62 | 7 | 23 | 92 |
| backend\\authenticationService | 18 | 4,801 | 16 | 88 | 4,905 |
| backend\\authenticationService (Files) | 8 | 4,444 | 14 | 38 | 4,496 |
| backend\\authenticationService\\src | 10 | 357 | 2 | 50 | 409 |
| backend\\authenticationService\\src\\configs | 2 | 19 | 0 | 1 | 20 |
| backend\\authenticationService\\src\\controllers | 1 | 100 | 0 | 10 | 110 |
| backend\\authenticationService\\src\\controllers\\auth | 1 | 100 | 0 | 10 | 110 |
| backend\\authenticationService\\src\\middlewares | 3 | 128 | 2 | 19 | 149 |
| backend\\authenticationService\\src\\models | 3 | 73 | 0 | 14 | 87 |
| backend\\authenticationService\\src\\routes | 1 | 37 | 0 | 6 | 43 |
| backend\\codeExecutionService | 27 | 4,819 | 111 | 287 | 5,217 |
| backend\\codeExecutionService (Files) | 6 | 3,498 | 4 | 28 | 3,530 |
| backend\\codeExecutionService\\configs | 1 | 16 | 0 | 1 | 17 |
| backend\\codeExecutionService\\controlers | 1 | 71 | 12 | 5 | 88 |
| backend\\codeExecutionService\\controlers\\submission | 1 | 71 | 12 | 5 | 88 |
| backend\\codeExecutionService\\docker | 6 | 418 | 17 | 91 | 526 |
| backend\\codeExecutionService\\docker (Files) | 1 | 228 | 11 | 45 | 284 |
| backend\\codeExecutionService\\docker\\language | 5 | 190 | 6 | 46 | 242 |
| backend\\codeExecutionService\\grpc | 1 | 19 | 0 | 8 | 27 |
| backend\\codeExecutionService\\middlewares | 3 | 83 | 9 | 17 | 109 |
| backend\\codeExecutionService\\models | 2 | 67 | 2 | 9 | 78 |
| backend\\codeExecutionService\\proto | 1 | 13 | 0 | 4 | 17 |
| backend\\codeExecutionService\\routes | 1 | 33 | 0 | 6 | 39 |
| backend\\codeExecutionService\\source | 2 | 9 | 0 | 4 | 13 |
| backend\\codeExecutionService\\source\\404 | 1 | 1 | 0 | 1 | 2 |
| backend\\codeExecutionService\\source\\706 | 1 | 8 | 0 | 3 | 11 |
| backend\\codeExecutionService\\workers | 3 | 592 | 67 | 114 | 773 |
| backend\\problemManageService | 28 | 5,628 | 30 | 180 | 5,838 |
| backend\\problemManageService (Files) | 7 | 4,724 | 6 | 29 | 4,759 |
| backend\\problemManageService\\src | 21 | 904 | 24 | 151 | 1,079 |
| backend\\problemManageService\\src\\configs | 2 | 28 | 0 | 2 | 30 |
| backend\\problemManageService\\src\\controllers | 5 | 476 | 8 | 75 | 559 |
| backend\\problemManageService\\src\\controllers\\auth | 1 | 77 | 0 | 8 | 85 |
| backend\\problemManageService\\src\\controllers\\categories | 1 | 11 | 0 | 4 | 15 |
| backend\\problemManageService\\src\\controllers\\languageSupport | 1 | 0 | 0 | 1 | 1 |
| backend\\problemManageService\\src\\controllers\\problems | 1 | 268 | 5 | 39 | 312 |
| backend\\problemManageService\\src\\controllers\\testcases | 1 | 120 | 3 | 23 | 146 |
| backend\\problemManageService\\src\\grpc | 2 | 68 | 0 | 11 | 79 |
| backend\\problemManageService\\src\\grpc (Files) | 1 | 32 | 0 | 8 | 40 |
| backend\\problemManageService\\src\\grpc\\testcase | 1 | 36 | 0 | 3 | 39 |
| backend\\problemManageService\\src\\middlewares | 4 | 121 | 9 | 24 | 154 |
| backend\\problemManageService\\src\\models | 5 | 121 | 7 | 18 | 146 |
| backend\\problemManageService\\src\\proto | 1 | 13 | 0 | 4 | 17 |
| backend\\problemManageService\\src\\routes | 2 | 77 | 0 | 17 | 94 |
| frontend | 92 | 20,808 | 75 | 552 | 21,435 |
| frontend (Files) | 11 | 16,250 | 6 | 31 | 16,287 |
| frontend\\public | 3 | 68 | 0 | 1 | 69 |
| frontend\\src | 78 | 4,490 | 69 | 520 | 5,079 |
| frontend\\src\\constants | 3 | 104 | 0 | 4 | 108 |
| frontend\\src\\model | 1 | 79 | 0 | 2 | 81 |
| frontend\\src\\models | 4 | 35 | 12 | 8 | 55 |
| frontend\\src\\network | 4 | 204 | 8 | 34 | 246 |
| frontend\\src\\network (Files) | 1 | 37 | 8 | 7 | 52 |
| frontend\\src\\network\\adminApi | 1 | 74 | 0 | 12 | 86 |
| frontend\\src\\network\\authApi | 1 | 18 | 0 | 5 | 23 |
| frontend\\src\\network\\problemApi | 1 | 75 | 0 | 10 | 85 |
| frontend\\src\\pages | 46 | 3,323 | 45 | 368 | 3,736 |
| frontend\\src\\pages (Files) | 3 | 51 | 1 | 7 | 59 |
| frontend\\src\\pages\\admin | 15 | 1,295 | 13 | 125 | 1,433 |
| frontend\\src\\pages\\admin (Files) | 2 | 129 | 0 | 20 | 149 |
| frontend\\src\\pages\\admin\\components | 2 | 113 | 3 | 12 | 128 |
| frontend\\src\\pages\\admin\\components\\FeatureBar | 1 | 53 | 3 | 7 | 63 |
| frontend\\src\\pages\\admin\\components\\ProblemList | 1 | 60 | 0 | 5 | 65 |
| frontend\\src\\pages\\admin\\idComponents | 11 | 1,053 | 10 | 93 | 1,156 |
| frontend\\src\\pages\\admin\\idComponents\\Detail | 5 | 445 | 0 | 46 | 491 |
| frontend\\src\\pages\\admin\\idComponents\\Detail (Files) | 1 | 134 | 0 | 11 | 145 |
| frontend\\src\\pages\\admin\\idComponents\\Detail\\components | 4 | 311 | 0 | 35 | 346 |
| frontend\\src\\pages\\admin\\idComponents\\Language | 2 | 314 | 2 | 14 | 330 |
| frontend\\src\\pages\\admin\\idComponents\\Language (Files) | 1 | 11 | 0 | 3 | 14 |
| frontend\\src\\pages\\admin\\idComponents\\Language\\components | 1 | 303 | 2 | 11 | 316 |
| frontend\\src\\pages\\admin\\idComponents\\Language\\components\\LanguageList | 1 | 303 | 2 | 11 | 316 |
| frontend\\src\\pages\\admin\\idComponents\\Setting | 1 | 7 | 0 | 1 | 8 |
| frontend\\src\\pages\\admin\\idComponents\\Testcase | 3 | 287 | 8 | 32 | 327 |
| frontend\\src\\pages\\admin\\idComponents\\Testcase (Files) | 1 | 45 | 8 | 6 | 59 |
| frontend\\src\\pages\\admin\\idComponents\\Testcase\\components | 2 | 242 | 0 | 26 | 268 |
| frontend\\src\\pages\\admin\\idComponents\\Testcase\\components\\TestcaseEdit | 1 | 114 | 0 | 16 | 130 |
| frontend\\src\\pages\\admin\\idComponents\\Testcase\\components\\TestcaseList | 1 | 128 | 0 | 10 | 138 |
| frontend\\src\\pages\\api | 1 | 3 | 1 | 2 | 6 |
| frontend\\src\\pages\\authentication | 3 | 364 | 3 | 36 | 403 |
| frontend\\src\\pages\\authentication (Files) | 1 | 140 | 1 | 17 | 158 |
| frontend\\src\\pages\\authentication\\components | 2 | 224 | 2 | 19 | 245 |
| frontend\\src\\pages\\components | 2 | 67 | 0 | 8 | 75 |
| frontend\\src\\pages\\components\\HistoryList | 1 | 36 | 0 | 3 | 39 |
| frontend\\src\\pages\\components\\UserAnalysis | 1 | 31 | 0 | 5 | 36 |
| frontend\\src\\pages\\problems | 22 | 1,543 | 27 | 190 | 1,760 |
| frontend\\src\\pages\\problems (Files) | 2 | 162 | 0 | 27 | 189 |
| frontend\\src\\pages\\problems\\components | 9 | 470 | 23 | 53 | 546 |
| frontend\\src\\pages\\problems\\components (Files) | 4 | 261 | 0 | 23 | 284 |
| frontend\\src\\pages\\problems\\components\\ProblemList | 5 | 209 | 23 | 30 | 262 |
| frontend\\src\\pages\\problems\\idComponent | 11 | 911 | 4 | 110 | 1,025 |
| frontend\\src\\pages\\problems\\idComponent\\console | 6 | 449 | 4 | 56 | 509 |
| frontend\\src\\pages\\problems\\idComponent\\console (Files) | 1 | 56 | 0 | 6 | 62 |
| frontend\\src\\pages\\problems\\idComponent\\console\\components | 5 | 393 | 4 | 50 | 447 |
| frontend\\src\\pages\\problems\\idComponent\\editor | 1 | 117 | 0 | 14 | 131 |
| frontend\\src\\pages\\problems\\idComponent\\problem | 3 | 289 | 0 | 32 | 321 |
| frontend\\src\\pages\\problems\\idComponent\\problem (Files) | 1 | 48 | 0 | 6 | 54 |
| frontend\\src\\pages\\problems\\idComponent\\problem\\components | 2 | 241 | 0 | 26 | 267 |
| frontend\\src\\pages\\problems\\idComponent\\problem\\components\\description | 1 | 66 | 0 | 6 | 72 |
| frontend\\src\\pages\\problems\\idComponent\\problem\\components\\submission | 1 | 175 | 0 | 20 | 195 |
| frontend\\src\\pages\\problems\\idComponent\\result | 1 | 56 | 0 | 8 | 64 |
| frontend\\src\\reducers | 9 | 371 | 3 | 49 | 423 |
| frontend\\src\\reducers (Files) | 1 | 16 | 0 | 5 | 21 |
| frontend\\src\\reducers\\appRoutes | 2 | 65 | 0 | 6 | 71 |
| frontend\\src\\reducers\\authentication | 3 | 82 | 0 | 10 | 92 |
| frontend\\src\\reducers\\problem | 3 | 208 | 3 | 28 | 239 |
| frontend\\src\\shared | 10 | 243 | 0 | 33 | 276 |
| frontend\\src\\shared (Files) | 1 | 7 | 0 | 3 | 10 |
| frontend\\src\\shared\\base | 3 | 21 | 0 | 2 | 23 |
| frontend\\src\\shared\\buttonTab | 1 | 26 | 0 | 6 | 32 |
| frontend\\src\\shared\\navbar | 2 | 146 | 0 | 16 | 162 |
| frontend\\src\\shared\\navbar (Files) | 1 | 127 | 0 | 12 | 139 |
| frontend\\src\\shared\\navbar\\components | 1 | 19 | 0 | 4 | 23 |
| frontend\\src\\shared\\utilities | 3 | 43 | 0 | 6 | 49 |
| frontend\\src\\shared\\utilities (Files) | 1 | 4 | 0 | 2 | 6 |
| frontend\\src\\shared\\utilities\\Logger | 1 | 6 | 0 | 1 | 7 |
| frontend\\src\\shared\\utilities\\MarkdownRender | 1 | 33 | 0 | 3 | 36 |
| frontend\\src\\styles | 1 | 131 | 1 | 22 | 154 |
| gateway(backup) | 6 | 203 | 0 | 18 | 221 |
| gateway(backup)\\version1-kong-with-db-and-konga | 2 | 98 | 0 | 8 | 106 |
| gateway(backup)\\version2-kong-dbless | 4 | 105 | 0 | 10 | 115 |
| gateway(backup)\\version2-kong-dbless (Files) | 3 | 47 | 0 | 7 | 54 |
| gateway(backup)\\version2-kong-dbless\\declarative | 1 | 58 | 0 | 3 | 61 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)