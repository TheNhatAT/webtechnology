# Diff Summary

Date : 2023-07-08 01:42:24

Directory c:\\Users\\minht\\OneDrive\\Desktop\\webtechnology

Total : 2 files,  -9 codes, 0 comments, -4 blanks, all -13 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Go | 1 | -1 | 0 | -1 | -2 |
| C++ | 1 | -8 | 0 | -3 | -11 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 2 | -9 | 0 | -4 | -13 |
| backend | 2 | -9 | 0 | -4 | -13 |
| backend\\codeExecutionService | 2 | -9 | 0 | -4 | -13 |
| backend\\codeExecutionService\\source | 2 | -9 | 0 | -4 | -13 |
| backend\\codeExecutionService\\source\\404 | 1 | -1 | 0 | -1 | -2 |
| backend\\codeExecutionService\\source\\706 | 1 | -8 | 0 | -3 | -11 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)